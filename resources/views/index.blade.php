﻿@extends('layouts.master-front')
@section('content')



    <div class="container-fluid">

        @if(session('error'))
            <div class="alert alert-warning">
                <strong>خطا!</strong> {{session('error')}}
            </div>
        @endif


        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="row text-center ">
            <div class="user-form col-10 col-sm-6 col-lg-5 ">
                <form action="{{route('singLetter')}}" class="text-center" method="get">
                    @csrf
                    <div id="error">
                        <p id="message"></p>
                    </div>
                    <div class="text-form">
                        <p>کاربر گرامی جهت تایید نامه اداری خود با استفاده از امضای همراه رایتل وارد شوید</p>
                    </div>
                    <div class="form-group ">
                        <div class="">
                            <input id="national_code" name="national_code" type="number"
                                   value="{{old('national_code')}}"
                                   pattern="[0-9]*" class="form-control item-form" minlength="10" maxlength="10"
                                   placeholder="نام کاربری (کد ملی)">
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="">
                            <input id="mobile" name="mobile" type="number" value="{{old('mobile')}}"
                                   class="form-control item-form" minlength="11" maxlength="11"
                                   placeholder="رمز عبور (شماره موبایل رایتل)">
                        </div>
                    </div>

                    <div class="form-group set-dir ">
                        <button type="submit" value="Submit" class="btn btn-primary item-form  buttom-form">
                            تایید نامه با امضای همراه رایتل
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="empty">

    </div>
@endsection
