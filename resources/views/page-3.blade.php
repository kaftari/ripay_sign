﻿@extends('layouts.master-front')
@section('content')
    <div class="container leeee">
        <div class="card">
            <div class="card-header">
                <div class="data">
                    <p>99/11/13</p>
                    <p>ST-99-1795</p>
                </div>
                <div class=" name">
                    <p>بسمه تعالی</p>
                </div>
            </div>
            <div class="card-body">
             @include('partials.letter')
            </div>
            <div class="card-footer">
                <div class="data">
                    <p>محمد محسن صدر</p>
                    <p>نایب رئیس هیأت مدیره و مدیر عامل</p>
                </div>

                <div class="im">
                    <img src="{{asset('front')}}/img/sign.png" alt="">
                </div>
            </div>
        </div>
        <div class="text-center">
            <button class=" end-message">
                نامه شما با استفاده از امضای همراه رایتل (پاراف) تایید گردید
                <span>
                    <img src="{{asset('front')}}/img/log.png" class="logo-paraf" alt="">
                </span>
            </button>
        </div>
    </div>
@endsection
