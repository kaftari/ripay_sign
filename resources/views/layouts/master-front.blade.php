<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{asset('front')}}/css/bootstrap..min.css">
    <link rel="stylesheet" href="{{asset('front')}}/css/all.css">
    <link rel="stylesheet" href="{{asset('front')}}/css/all.min.css">
    <link rel="stylesheet" href="{{asset('front')}}/css/style.css">
    {{--<link rel="stylesheet" id="the7-awesome-fonts-css"
          href="https://irightel.ir/wp-content/themes/dt-the7/fonts/FontAwesome/css/all.min.css?ver=8.5.0.2"
          type="text/css" media="all">--}}
    <title>امضای همراه رایتل</title>
    <link rel="icon" type="image/png" href="{{asset('front')}}/img/favicon.png">
    <script src="{{asset('front')}}/js/jquery.min.js"></script>

    <script src="{{asset('front')}}/js/javaInput.js"></script>
    <script src="{{asset('front')}}/js/bootstrap.min.js"></script>
</head>

<body>

<div id="wrap">
    <div class="header">
        <nav class="navbar navbar-expand navbar-light navbar-color sticky-top navbarssss">
            <div class="container-fluid container-fluidsss">

                <a href="" class="navbar-brand text">
                    <i class="fas fa-caret-left"></i>
                    <span>
نوآوری ، سرعت، دانش
            </span></a>
                <button type="button" class="navbar-toggler" data-toggle="collapse"
                        data-target="#navbarResponsive">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav  ">
                        <li class="links nav-item active">
                            <div class=""><a class="fab fa-instagram" href=""></a>
                        <li class="links nav-item">
                            <div class=""><a class="fas fa-wifi" href=""></a></div>
                        </li>
                        <li class="links nav-item">
                            <div class=""><a class="fab fa-linkedin-in" href=""></a></div>
                        </li>
                        <li class="links nav-item">
                            <div class=""><a class="fab fa-twitter" href=""></a></div>
                        </li>
                        <li class="links nav-item">
                            <div class=""><a class="fab fa-facebook-f" href=""></a></div>
                        </li>
                    </ul>

                </div>
        </nav>


        <header class="header-bar">

            <div class="branding">
                <ul class="brand-list ">
                    <li class="brand-Icon ">
                        <img class="img1" src="{{asset('front')}}/img/rightel_2.png" alt="">
                    </li>
                    <li class="brand-Icon">
                        <img class="img2" src="{{asset('front')}}/img/Rightel_Icon.png" alt="">
                    </li>
                    <li class="brand-Icon ">
                        <img class="img1" src="{{asset('front')}}/img/ayandeh_Icon.png" alt="">
                    </li>
                    <li class="text-list">
                        سامانه امضای همراه رایتل (پاراف)
                    </li>
                </ul>
            </div>
            <hr>
            <nav class="navigation">
                <ul class="sssss">
                    <li class="home">
                        <a href="https://irightel.ir/" data-level="1">
                            <i class="icon-home">
                                <i class="fas fa-home"></i>
                            </i>
                            <span class="">خانه</span>
                        </a>
                    </li>
                </ul>
            </nav>
        </header>

        <div class="menu-bar">
        <span class="right-text">امضای همراه
        </span>
            <span class="left-text">
            <a href="{{route('index')}}">صفحه نخست</a>
            <i>/امضای همراه</i>
        </span>
        </div>
    </div>

    @yield('content')

</div>

<!-- ...................................................................................... -->
<!-- ...................................................................................... -->

<footer class="footer-area ">
    <div class="container-fluid">
        <div class=" row justify-content-md-center">

            <div class="col-12 col-sm-4  col-lg-3  brand-footer"><img src="{{asset('front')}}/img/rightel_3.png" alt=""></div>
            <div class="col-12 col-sm-4  col-lg-3 address">
                <p class="b">اطلاعات تماس</p>
                <p>آدرس:تهران -سعادت آباد، خیابان حق طلب غربی(26) پلاک 47، طبقه سوم شماره تماس: 43650000-021 </p>
                <div class="set-dir-ltr">
                    <ul class="navbar-nav-bottom ">
                        <li class="links nav-item active">
                            <div class=""><a class="fab fa-instagram" href=""></a>
                        <li class="links nav-item">
                            <div class=""><a class="fas fa-wifi" href=""></a></div>
                        </li>
                        <li class="links nav-item">
                            <div class=""><a class="fab fa-linkedin-in" href=""></a></div>
                        </li>
                        <li class="links nav-item">
                            <div class=""><a class="fab fa-twitter" href=""></a></div>
                        </li>
                        <li class="links nav-item">
                            <div class=""><a class="fab fa-facebook-f" href=""></a></div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-12 col-sm-4 col-lg-3  brand-footer">
                <div class="textwidget custom-html-widget">
                    <a href="https://trustseal.enamad.ir/?id=197313&amp;Code=8wKyTNdg1sjnAbURYZFP">
                        {{--<img referrerpolicy="origin" src="https://irightel.ir/wp-content/uploads/2020/12/logo.png"
                             alt="" style="cursor:pointer" id="8wKyTNdg1sjnAbURYZFP">--}}
                    </a>
                </div>
            </div>
            <!-- <div class="col-sm-6 col-lg-3  brand-footer">
                <img id="nbqejxlzjxlznbqenbqeoeuk" style="cursor:pointer" onclick='window.open("https://logo.samandehi.ir/Verify.aspx?id=211228&amp;p=uiwkrfthrfthuiwkuiwkmcsi&quot;",";toolbar=no, scrollbars=no, location=no, statusbar=no, menubar=no, resizable=0, width=450, height=630, top=30&quot;")' alt="logo-samandehi" src="https://logo.samandehi.ir/logo.aspx?id=211228&amp;p=odrfnbpdnbpdodrfodrfaqgw">
            </div> -->

        </div>
        <hr>
        <div class="end-line">
            <div class="branding-bottom">
                {{--<a class="" href="https://irightel.ir/"><img class=" preload-me"
                                                             src="https://irightel.ir/wp-content/uploads/2020/10/l-f.png"
                                                             srcset="https://irightel.ir/wp-content/uploads/2020/10/l-f.png 23w, https://irightel.ir/wp-content/uploads/2020/10/l-f-hd.png 46w"
                                                             width="23" height="26" sizes="23px"
                                                             alt="شرکت اطلاع رسان رایتل">
                </a>--}}
            </div>
            <p class="endP">© تمام حقوق سایت برای شرکت اطلاع رسان رایتل محفوظ است.

            </p>
        </div>

    </div>

</footer>

<script src="{{asset('front')}}/js/javaInput.js"></script>
<script src="{{asset('front')}}/js/bootstrap.min.js"></script>
</body>
</html>
