﻿@extends('layouts.master-front')
@section('content')

    <div class="container leeee">

        @if (session('error'))
            <div class="alert alert-danger">
                <ul>
                    <li>{{ session('error') }}</li>
                </ul>
            </div>
        @endif

        <div class="card">
            <div class="card-header">
                <div class="data">
                    <p>99/11/13</p>
                    <p>ST-99-1795</p>
                </div>
                <div class=" name">
                    <p>بسمه تعالی</p>
                </div>
            </div>
            <div class="card-body">
             @include('partials.letter')
            </div>
            <div class="card-footer">
                <div class="data">
                    <p>محمد محسن صدر</p>
                    <p>نایب رئیس هیأت مدیره و مدیر عامل</p>
                </div>

            </div>
        </div>
        <div class="text-center">
            <div class="btn-letter ">
                {{--                <button class="btn btn-primary" onclick="sendoky()">نامه مورد تایید می باشد</button>--}}
                <a class="btn btn-primary" href="{{route('signVerifyLetter')}}">دریافت تاییدیه برای امضای نامه</a>
            </div>
        </div>
    </div>
@endsection
