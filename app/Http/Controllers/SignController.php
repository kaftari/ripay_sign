<?php


namespace App\Http\Controllers;


use App\Models\Sign;
use DOMDocument;
use Illuminate\Http\Request;
use SimpleXMLElement;

class SignController extends Controller
{
    public function sign(Request $request)
    {
        $request->validate([
            'national_code' => 'required|min:10|max:10',
            'mobile' => 'required|min:11|max:11|regex:/(0)[0-9]{9}/',
        ]);

        $phone_valid = Sign::where('phone',$request->mobile)
            ->where('national_code',$request->national_code)
            ->first();

        if (!$phone_valid)
            return redirect()->back()->with('error','شماره ملی و شماره همراه وارد شده با اطلاعات موجود در سامانه مطابقت ندارد!');

        return view('page-2');
    }

    public function sign_api()
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'http://81.91.133.19:80/ap/MidpSignService',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => '<?xml version="1.0" encoding="UTF-8"?>
<soapenv:Envelope
	xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
	xmlns:HOMEMIDP="http://www.titmobileid.ir/titr_homemidp-g1/">
	<soapenv:Header/>
	<soapenv:Body>
		<HOMEMIDP:MSS_SignatureReq MajorVersion="1" MinorVersion="1" ValidityDate="2021-05-10T15:58:27.896+03:30" TimeOut="200" MessagingMode="synch">
			<AP_Info AP_ID="TestAP1" AP_TransID="_100" AP_PWD="Password1@" Instant="2021-05-10T15:58:27.896+03:30"/>
			<HOMEMIDP_Info>
				<HOMEMIDP_ID>
					<IPAddress>127.0.0.1</IPAddress>
				</HOMEMIDP_ID>
			</HOMEMIDP_Info>
			<MobileUser>
				<MSISDN>989200403262</MSISDN>
			</MobileUser>
			<DataToBeSigned MimeType="text/plain" Encoding="8bit">لطفا نامه را امضا نمایید</DataToBeSigned>
			<DataToBeDisplayed>لطفا درخواست را امضا نماييد</DataToBeDisplayed>
			<KeyReference>
				<CertificateURL>http://mss.homemidp.ir/?keyId=b063d3e7-6d1a-42b4-aa66-9d35ce90778a</CertificateURL>
			</KeyReference>
			<SignatureProfileComparison>better</SignatureProfileComparison>
		</HOMEMIDP:MSS_SignatureReq>
	</soapenv:Body>
</soapenv:Envelope>',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: text/xml',
                'Cookie: JSESSIONID=H7JvsGG2I59KsqLYu5mo4hNmfbtDQs_mN7x0_rtuAXZmba_-tf7u!-138696107'
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        $doc = new DOMDocument('1.0', 'utf-8');
        $doc->loadXML( $response );
        $XMLresults     = $doc->getElementsByTagName("Status");
        $output = $XMLresults->item(0)->nodeValue;

        if ($output == 'VALID_SIGNATURE')
            return view('page-3');

        return redirect()->back()->with('error','پاسخی از سامانه احراز هویت امضاء دریافت نشد!');
    }
}
